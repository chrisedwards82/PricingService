# PricingService

ActiveMQ will need to be installed on your local machine in order to run the
PricingService Application. To do this...

- Download ActiveMQ from https://activemq.apache.org/components/classic/download/
- Unzip contents to desired location.
- Add /bin directory to your PATH
- run 'activemq start' from your console
- Go to http://localhost:8161/
- Click manage ActiveMQ Broker
- Add topics /MIZUHO/PRICEDATA/REUTERS/ and /MIZUHO/PRICEDATA/BLOOMBERG/

Build the PricingService

Run com.mizuho.pricingservice.Application

Test Service is running by going to:
http://localhost:8080/swagger-ui.html

Use the Swagger UI to insert test data. Test data can be found:
PricingService\src\test\resources\test-data.json

Paste this data into the /price-service/ POST REST data call.

This data can now be retrieved using the GET REST calls.

