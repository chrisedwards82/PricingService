package com.mizuho.pricingservice.dao;

import com.mizuho.pricingservice.model.PriceData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;


@RunWith(SpringRunner.class)
public class PriceDataCacheTest {

    @TestConfiguration
    static class PriceDataCacheTestContextConfiguration {

        @Bean
        public Cache<PriceData> priceDataCache() { return new PriceDataCache(); }
    }

    @Autowired
    private Cache<PriceData> priceDataCache;

    @MockBean
    ConcurrentHashMap<Long, ConcurrentHashMap<Long, PriceData>> cache;

    @Before
    public void setUp() {
        PriceData pd1 = new PriceData(1001L, "Vodafone", 1001L, "Bloomberg", 10.45D);
        PriceData pd2 = new PriceData(1002L, "Apple", 1001L, "Bloomberg", 35.78D);
        PriceData pd3 = new PriceData(1001L, "Vodafone", 1002L, "Reuters", 10.85D);
        PriceData pd4 = new PriceData(1002L, "Apple", 1002L, "Reuters", 35.12D);

        List<PriceData> resultList1 = new ArrayList<>();
        resultList1.add(pd1);

        ConcurrentHashMap<Long, PriceData> testMap1 = new ConcurrentHashMap<>();
        testMap1.put(1001L, pd1);
        testMap1.put(1002L, pd2);

        List<PriceData> result = new ArrayList<>();

//        Mockito.when(cache.get(pd1.getVendorId()).get(pd1.getInstrumentId()))
//                .thenReturn(pd1);
//        Mockito.when(cache.values())
//                .thenReturn((Collection<ConcurrentHashMap<Long, PriceData>>) testMap1);
    }

    @Test
    public void getByInstrument() {
        Long id = 1001L;
        PriceData result = cache.get(id).get(id);

        assertThat(result.getInstrumentName(), equalTo("Vodafone"));
    }



}
