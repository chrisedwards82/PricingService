package com.mizuho.pricingservice.service;

import com.mizuho.pricingservice.dao.PriceDataDao;
import com.mizuho.pricingservice.services.publishers.MQPricePublisher;
import com.mizuho.pricingservice.model.PriceData;
import com.mizuho.pricingservice.services.PriceRequestService;
import com.mizuho.pricingservice.services.PriceService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.equalTo;


@RunWith(SpringRunner.class)
public class PriceRequestServiceIntegrationTest {

    @TestConfiguration
    static class PriceRequestServiceTestContextConfiguration {

        @Bean
        public PriceService priceService() {
            return new PriceRequestService();
        }
    }

    @Autowired
    private PriceService priceService;

    @MockBean
    @Qualifier("priceDataDaoImpl")
    private PriceDataDao<PriceData> priceDataDao;

    @MockBean
    private MQPricePublisher pricePublisher;

    @Before
    public void setUp() {
        PriceData pd1 = new PriceData(1001L, "Vodafone", 1001L, "Bloomberg", 10.45D);
        PriceData pd2 = new PriceData(1002L, "Apple", 1001L, "Bloomberg", 35.78D);
        PriceData pd3 = new PriceData(1001L, "Vodafone", 1002L, "Reuters", 10.85D);
        PriceData pd4 = new PriceData(1002L, "Apple", 1002L, "Reuters", 35.12D);

        List<PriceData> resultList1 = new ArrayList<>();
        resultList1.add(pd1);
        List<PriceData> resultList2 = new ArrayList<>();
        resultList2.add(pd1);
        resultList2.add(pd2);
        List<PriceData> resultList3 = new ArrayList<>();
        resultList3.add(pd1);
        resultList3.add(pd2);
        resultList3.add(pd3);
        resultList3.add(pd4);

        Mockito.when(priceDataDao.getByInstrument(pd1.getInstrumentId()))
                .thenReturn(resultList1);
        Mockito.when(priceDataDao.getByVendor(pd1.getVendorId()))
                .thenReturn(resultList2);
        Mockito.when(priceDataDao.getAll())
                .thenReturn(resultList3);
    }

    @Test
    public void getByInstrumentWithValidId() {
        Long id = 1001L;
        List<PriceData> result = priceDataDao.getByInstrument(id);

        assertThat(result.size(), greaterThan(0));
        assertThat(result.get(0).getInstrumentId(), equalTo(1001L));
    }

    @Test
    public void getByInstrumentWithNonMatchingId() {
        Long id = 9999L;
        List<PriceData> result = priceDataDao.getByInstrument(id);

        assertThat(result.size(), equalTo(0));
    }

    @Test
    public void getByInstrumentWithNullId() {
        Long id = null;
        List<PriceData> result = priceDataDao.getByInstrument(id);

        assertThat(result.size(), equalTo(0));
    }

    @Test
    public void getByVendorWithValidId() {
        Long id = 1001L;
        List<PriceData> result = priceDataDao.getByVendor(id);

        assertThat(result.size(), equalTo(2));
    }

    @Test
    public void getByVendorWithNonMatchingId() {
        Long id = 9999L;
        List<PriceData> result = priceDataDao.getByVendor(id);

        assertThat(result.size(), equalTo(0));
    }

    @Test
    public void getByVendorWithNullId() {
        Long id = null;
        List<PriceData> result = priceDataDao.getByVendor(id);

        assertThat(result.size(), equalTo(0));
    }

    @Test
    public void getAll() {
        List<PriceData> result = priceDataDao.getAll();

        assertThat(result.size(), equalTo(4));
    }
}
