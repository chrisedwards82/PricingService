package com.mizuho.pricingservice.config;

import javax.jms.ConnectionFactory;
import javax.jms.Topic;

import com.mizuho.pricingservice.services.consumers.MQBloombergSubscriber;
import com.mizuho.pricingservice.services.consumers.MQReutersSubscriber;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.listener.MessageListenerContainer;

@Configuration
@PropertySource(value = "classpath:application.properties")
public class JmsConfig {

    @Autowired
    private Environment env;

    @Autowired
    private MQBloombergSubscriber bloombergSubscriber;

    @Autowired
    private MQReutersSubscriber reutersSubscriber;

    @Bean
    public ConnectionFactory connectionFactory() {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(env.getProperty("JMS.BROKER.URL"));
        return connectionFactory;
    }

    @Bean
    public CachingConnectionFactory cachingConnectionFactory() {
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory(connectionFactory());
        return cachingConnectionFactory;
    }

    @Bean (name="JMS.TOPIC.REUTERS")
    public Topic reutersTopic() {
        Topic topic = new ActiveMQTopic(env.getProperty("JMS.TOPIC.REUTERS"));
        return topic;
    }

    @Bean (name="JMS.TOPIC.BLOOMBERG")
    public Topic bloombergTopic() {
        Topic topic = new ActiveMQTopic(env.getProperty("JMS.TOPIC.BLOOMBERG"));
        return topic;
    }

    @Bean
    public JmsTemplate jmsTemplate() {
        JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory());
        jmsTemplate.setPubSubDomain(true);

        return jmsTemplate;
    }

    @Bean (name = "bloombergContainer")
    public MessageListenerContainer messageListenerContainerOne() {
        DefaultMessageListenerContainer messageListenerContainer = new DefaultMessageListenerContainer();

        messageListenerContainer.setPubSubDomain(true);
        messageListenerContainer.setDestination(bloombergTopic());
        messageListenerContainer.setMessageListener(bloombergSubscriber);
        messageListenerContainer.setConnectionFactory(connectionFactory());

        return messageListenerContainer;
    }

    @Bean(name = "reutersContainer")
    public MessageListenerContainer messageListenerContainerTwo() {
        DefaultMessageListenerContainer messageListenerContainer = new DefaultMessageListenerContainer();

        messageListenerContainer.setPubSubDomain(true);
        messageListenerContainer.setDestination(reutersTopic());
        messageListenerContainer.setMessageListener(reutersSubscriber);
        messageListenerContainer.setConnectionFactory(connectionFactory());

        return messageListenerContainer;
    }

}
