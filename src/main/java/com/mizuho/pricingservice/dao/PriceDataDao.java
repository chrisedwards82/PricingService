package com.mizuho.pricingservice.dao;

import java.util.List;

public interface PriceDataDao<T> {

    Boolean put(T item);

    List<T> getAll();

    List<T> getByVendor(Long vendorId);

    List<T> getByInstrument(Long instrumentId);
}
