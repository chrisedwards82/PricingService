package com.mizuho.pricingservice.dao;

import org.apache.log4j.Logger;

import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

abstract class Cache<T> extends TimerTask implements PriceDataDao<T> {

    final static Logger log = Logger.getLogger(Cache.class);

    protected ConcurrentHashMap<Long, ConcurrentHashMap<Long, T>> cache;

    abstract void purgeStaleEntries();

    public Cache() {
        this.cache = new ConcurrentHashMap<>();

        Timer timer = new Timer("CachePurgeTimer");
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 6);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);

        //Run task every morning at 6am
        timer.schedule(this, today.getTime(), TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS));
    }

    @Override
    public void run() {
        log.info("Purging stale Cache entries...");
        purgeStaleEntries();
    }
}
