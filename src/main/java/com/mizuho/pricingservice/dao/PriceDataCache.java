package com.mizuho.pricingservice.dao;

import com.mizuho.pricingservice.model.Instrument;
import com.mizuho.pricingservice.model.Price;
import com.mizuho.pricingservice.model.PriceData;
import com.mizuho.pricingservice.model.Vendor;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


@Component
public class PriceDataCache extends Cache<PriceData> {

    final static Logger log = Logger.getLogger(PriceDataCache.class);

    public PriceDataCache() {
        super();
    }

    @Override
    public Boolean put(PriceData pd) {
        cache.putIfAbsent(pd.getVendorData().getId(), new ConcurrentHashMap<>());

        PriceData oldValue = cache.get(pd.getVendorData().getId()).get(pd.getInstrumentData().getId());

        if (oldValue == null)
            cache.get(pd.getVendorData().getId()).put(pd.getInstrumentData().getId(), pd);
        else
            cache.get(pd.getVendorData().getId()).replace(pd.getInstrumentData().getId(), oldValue, pd);

        return true;
    }

    @Override
    public List<PriceData> getAll() {

        List<PriceData> result = new ArrayList<>();

        cache.forEach((k,v) -> result.addAll(v.values()));

        return result;
    }

    @Override
    public List<PriceData> getByVendor(Long vendorId) {

        List<PriceData> result = new ArrayList<>();
        if (!cache.containsKey(vendorId))
            return result;

        result.addAll(cache.get(vendorId).values());

        return result;
    }

    @Override
    public List<PriceData> getByInstrument(Long instrumentId) {
        List<PriceData> result = new ArrayList<>();

        for(ConcurrentHashMap<Long, PriceData> instruments : cache.values()) {
            if(instruments.containsKey(instrumentId)) {
                result.add(instruments.get(instrumentId));
            }
        }
        return result;
    }

    @Override
    protected void purgeStaleEntries() {
        if (cache.isEmpty())
            return;

        for (ConcurrentHashMap<Long, PriceData> instruments : cache.values()) {
            for (PriceData pd : instruments.values()) {
                if(pd.isStale()) {
                    cache.get(pd.getVendorData().getId()).remove(pd.getInstrumentData().getId(), pd);
                }
            }
        }
    }

}
