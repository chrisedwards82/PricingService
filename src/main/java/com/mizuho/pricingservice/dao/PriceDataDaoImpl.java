package com.mizuho.pricingservice.dao;

import com.mizuho.pricingservice.model.PriceData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("priceDataDaoImpl")
public class PriceDataDaoImpl implements PriceDataDao<PriceData> {

    final static Logger log = Logger.getLogger(PriceDataDaoImpl.class);

    @Autowired
    private PriceDataCache cache;

    @Override
    public List<PriceData> getAll() {
        return cache.getAll();
    }

    @Override
    public List<PriceData> getByVendor(Long vendorId) {
        return cache.getByVendor(vendorId);
    }

    @Override
    public List<PriceData> getByInstrument(Long instrumentId) {
        return cache.getByInstrument(instrumentId);
    }

    @Override
    public Boolean put(PriceData pd) {

        if (cache.put(pd)) {
            log.info("Inserted Price: " + pd.toString());
            return true;
        }
        return false;
    }
}
