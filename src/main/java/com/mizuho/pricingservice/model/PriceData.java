package com.mizuho.pricingservice.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.Date;

/**
 * Used for Rest Json serialization/de-serialization
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "instrumentId",
        "instrumentName",
        "vendorId",
        "vendorName",
        "price",
        "lastUpdated",
        "stale"
})
public class PriceData implements Serializable {

    @JsonIgnore
    private Instrument instrumentData;

    @JsonIgnore
    private Vendor vendorData;

    @JsonIgnore
    private Price priceData;

    public PriceData(Long instrumentId, String instrumentName, Long vendorId, String vendorName, Double price) {

        this.instrumentData = new Instrument(instrumentId, instrumentName);
        this.vendorData = new Vendor(vendorId, vendorName);
        this.priceData = new Price(instrumentId, vendorId, price);
    }

    public Instrument getInstrumentData() {
        return instrumentData;
    }

    public void setInstrumentData(Instrument instrumentData) {
        this.instrumentData = instrumentData;
    }

    public Vendor getVendorData() {
        return vendorData;
    }

    public void setVendorData(Vendor vendorData) {
        this.vendorData = vendorData;
    }

    public Price getPriceData() {
        return priceData;
    }

    public void setPriceData(Price priceData) {
        this.priceData = priceData;
    }

    @JsonGetter("instrumentId")
    public Long getInstrumentId() {
        return this.getInstrumentData().getId();
    }

    @JsonGetter("instrumentName")
    public String getInstrumentName() {
        return this.getInstrumentData().getName();
    }

    @JsonGetter("vendorId")
    public Long getVendorId() {
        return this.getVendorData().getId();
    }

    @JsonGetter("vendorName")
    public String getVendorName() {
        return this.getVendorData().getName();
    }

    @JsonGetter("price")
    public Double getPrice() {
        return this.getPriceData().getPrice();
    }

    @JsonGetter("lastUpdated")
    public Date getLastUpdated() {
        return this.getPriceData().getLastUpdated();
    }

    @JsonGetter("stale")
    public boolean isStale() {
        return this.getPriceData().isStale();
    }

    @Override
    public String toString() {
        return "PriceData{" +
                "instrumentData=" + instrumentData.toString() +
                ", vendorData=" + vendorData.toString() +
                ", priceData=" + priceData.toString() +
                '}';
    }
}
