package com.mizuho.pricingservice.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;


public class Vendor implements Serializable {

    private Long id;

    private String name;

    public Vendor(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Vendor{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
