package com.mizuho.pricingservice.model;

import java.util.Date;
import java.util.Objects;

public class Price {

    private static final Long EXPIRY_TIME = 2592000000L; //30 days in millis

    private Long id;

    private Long instrumentId;

    private Long vendorId;

    private Double price;

    private Date lastUpdated;

    private boolean stale;

    public Price(Long instrumentId, Long vendorId, Double price) {
        this.id = generatePK(instrumentId, vendorId);
        this.instrumentId = instrumentId;
        this.vendorId = vendorId;
        this.price = price;
        this.lastUpdated = new Date();
        this.stale = isStale();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getInstrumentId() {
        return instrumentId;
    }

    public void setInstrumentId(Long instrumentId) {
        this.instrumentId = instrumentId;
    }

    public Long getVendorId() {
        return vendorId;
    }

    public void setVendorId(Long vendorId) {
        this.vendorId = vendorId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public boolean isStale() {
        if (System.currentTimeMillis() - lastUpdated.getTime() > EXPIRY_TIME ) {
            return true;
        }
        return false;
    }

    public void setStale(boolean stale) {
        this.stale = stale;
    }

    private Long generatePK(Long instrumentId, Long vendorId) {
        return new Long(Objects.hash(instrumentId, vendorId));
    }


    @Override
    public String toString() {
        return "Price{" +
                "id=" + id +
                ", instrumentId=" + instrumentId +
                ", vendorId=" + vendorId +
                ", price=" + price +
                ", lastUpdated=" + lastUpdated +
                ", stale=" + stale +
                '}';
    }
}
