package com.mizuho.pricingservice.rest;

import com.mizuho.pricingservice.services.PriceRequestService;
import com.mizuho.pricingservice.model.PriceData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("price-service")
public class PricingController {

    @Autowired
    private PriceRequestService requestService;

    @GetMapping(value="/vendor/{id}", produces="application/json")
    public ResponseEntity<List<PriceData>> getByVendor(@PathVariable long id) {
        List<PriceData> result = requestService.getByVendor(id);
        return new ResponseEntity<List<PriceData>>(result, HttpStatus.OK);
    }

    @GetMapping(value="/instrument/{id}", produces="application/json")
    public ResponseEntity<List<PriceData>> getByInstrument(@PathVariable long id) {
        List<PriceData> result = requestService.getByInstrument(id);
        return new ResponseEntity<List<PriceData>>(result, HttpStatus.OK);
    }

    @GetMapping(value="/", produces="application/json")
    public ResponseEntity<List<PriceData>> getAll() {
        List<PriceData> result = requestService.getAll();
        return new ResponseEntity<List<PriceData>>(result, HttpStatus.OK);
    }

    @PostMapping(value="/", produces="application/json" , consumes="application/json")
    public ResponseEntity<HttpStatus> insertPrices(@RequestBody List<PriceData> prices) {
        if (requestService.insertPriceData(prices)) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


}
