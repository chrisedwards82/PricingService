package com.mizuho.pricingservice.services.publishers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mizuho.pricingservice.model.PriceData;
import com.mizuho.pricingservice.utility.BeanUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.jms.Topic;

@Component
@PropertySource(value = "classpath:application.properties")
public class MQPricePublisher {

    final static Logger log = Logger.getLogger(MQPricePublisher.class);

    @Autowired
    private Environment env;

    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendMessage(final String destination, final PriceData pd) {
        Topic t = (Topic)BeanUtil.getBean(destination);
        String payload = toJson(pd);

        jmsTemplate.convertAndSend(t, payload);

        log.info("Price published to: " + destination + " : " + payload);
    }

    private String toJson(PriceData pd) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(pd);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
