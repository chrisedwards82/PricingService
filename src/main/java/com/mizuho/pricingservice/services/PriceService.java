package com.mizuho.pricingservice.services;

import com.mizuho.pricingservice.model.PriceData;

import java.util.List;

public interface PriceService {

    List<PriceData> getAll();

    List<PriceData> getByVendor(Long vendorId);

    List<PriceData> getByInstrument(Long instrumentId);

    Boolean insertPriceData(List<PriceData> prices);

    void publishPrice(PriceData pd);
}
