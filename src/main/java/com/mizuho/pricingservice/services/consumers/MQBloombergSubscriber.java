package com.mizuho.pricingservice.services.consumers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mizuho.pricingservice.services.publishers.MQPricePublisher;
import com.mizuho.pricingservice.model.PriceData;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.io.IOException;

@Component
public class MQBloombergSubscriber implements MessageListener {

    final static Logger log = Logger.getLogger(MQPricePublisher.class);

    @Override
    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
            try {
                String msg = ((TextMessage) message).getText();
                log.info("Price consumed by Bloomberg Subscriber: " + msg);
            } catch (JMSException ex) {
                log.error("Failed to retrieve message", ex);
            }
        } else {
            log.error("Received message not of type TextMessage, ignoring...");
        }
    }

    private PriceData fromJson(String json) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(json, PriceData.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
