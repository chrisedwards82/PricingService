package com.mizuho.pricingservice.services;

import com.mizuho.pricingservice.dao.PriceDataDao;
import com.mizuho.pricingservice.services.publishers.MQPricePublisher;
import com.mizuho.pricingservice.model.PriceData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PriceRequestService implements PriceService {

    final static Logger log = Logger.getLogger(PriceRequestService.class);

    @Autowired
    @Qualifier("priceDataDaoImpl")
    private PriceDataDao priceDataDao;

    @Autowired
    private MQPricePublisher pricePublisher;

    @Override
    public List<PriceData> getAll() {
        return priceDataDao.getAll();
    }

    @Override
    public List<PriceData> getByVendor(Long vendorId) {
        return priceDataDao.getByVendor(vendorId);
    }

    @Override
    public List<PriceData> getByInstrument(Long instrumentId) {
        return priceDataDao.getByInstrument(instrumentId);
    }

    @Override
    public Boolean insertPriceData(List<PriceData> prices) {
        try {
            for (PriceData pd : prices) {
                if (!priceDataDao.put(pd)) {
                    return false;
                }
                publishPrice(pd);
            }
        } catch (Exception ex) {
            log.error("Error inserting price " + priceDataDao.toString(), ex);
            return false;
        }
        return true;
    }

    @Override
    public void publishPrice(PriceData pd) {

        //Build destination topic based on vendor
        String destination = "JMS.TOPIC.".concat(pd.getVendorData().getName().toUpperCase());
        pricePublisher.sendMessage(destination, pd);

    }
}
